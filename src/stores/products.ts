import { ref, watch } from "vue";
import { defineStore } from "pinia";
import type Product from "@/types/Product";
import productService from "@/services/product";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";

export const useProductStore = defineStore("Product", () => {
  const products = ref<Product[]>([]);
  const editedProducts = ref<Product>({ name: "", price: 0 });
  const dialog = ref(false);
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();

  watch(dialog, (newDialog, oldDialog) => {
    if (!newDialog) {
      editedProducts.value = { name: "", price: 0 };
    }
  });

  async function getProducts() {
    loadingStore.isLoading = true;
    try {
      const res = await productService.getProducts();
      products.value = res.data;
      // console.log(res);
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล Product ได้");
    }
    loadingStore.isLoading = false;
  }

  async function saveProduct() {
    try {
      if (editedProducts.value.id) {
        const res = await productService.updateProduct(
          editedProducts.value.id,
          editedProducts.value
        );
      } else {
        const res = await productService.saveProduct(editedProducts.value);
      }
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถบันทึกข้อมูลได้");
    }
    await getProducts();
    dialog.value = false;
  }

  function editProduct(product: Product) {
    dialog.value = true;
    editedProducts.value = JSON.parse(JSON.stringify(product));
  }

  async function deleteProduct(id: number) {
    try {
      const res = await productService.deleteProduct(id);
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบข้อมูลได้");
    }
    await getProducts();
  }
  return {
    products,
    getProducts,
    dialog,
    editedProducts,
    saveProduct,
    editProduct,
    deleteProduct,
  };
});
