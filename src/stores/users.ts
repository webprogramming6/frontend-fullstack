import { ref, watch } from "vue";
import { defineStore } from "pinia";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";
import type User from "@/types/User";
import orderService from "@/services/user";

export const useUserStore = defineStore("User", () => {
  const users = ref<User[]>([]);
  const editedUsers = ref<User>({ login: "", name: "", password: "" });
  const dialog = ref(false);
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();

  watch(dialog, (newDialog, oldDialog) => {
    if (!newDialog) {
      editedUsers.value = { login: "", name: "", password: "" };
    }
  });

  async function getUsers() {
    loadingStore.isLoading = true;
    try {
      const res = await orderService.getUsers();
      users.value = res.data;
      // console.log(res);
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล  User ได้");
    }
    loadingStore.isLoading = false;
  }

  async function saveUsers() {
    try {
      if (editedUsers.value.id) {
        const res = await orderService.updateUser(
          editedUsers.value.id,
          editedUsers.value
        );
      } else {
        const res = await orderService.saveUser(editedUsers.value);
      }
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถบันทึกข้อมูลได้");
    }
    await getUsers();
    dialog.value = false;
  }

  function editUsers(user: User) {
    dialog.value = true;
    editedUsers.value = JSON.parse(JSON.stringify(user));
  }

  async function deleteUsers(id: number) {
    try {
      const res = await orderService.deleteUser(id);
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบข้อมูลได้");
    }
    await getUsers();
  }
  return {
    users,
    getUsers,
    dialog,
    editedUsers,
    saveUsers,
    editUsers,
    deleteUsers,
  };
});
